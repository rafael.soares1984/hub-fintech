package br.hub.fintech;

import br.hub.fintech.entity.Card;
import br.hub.fintech.entity.Transactions;
import br.hub.fintech.repository.CardRespository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.time.LocalDateTime;
import java.util.Arrays;


/**
 * @author Rafael Soares
 *
 */
@SpringBootApplication
@EntityScan("br.hub.fintech.entity")
@ComponentScan(basePackages = "br.hub.fintech.*")
public class FintechApplication  {

    public static void main(String[] args) {
        SpringApplication.run(FintechApplication.class, args);
    }



    @Bean
    public CommandLineRunner init (CardRespository repository) {
        return args -> repository.saveAll(Arrays.asList(
                Card.builder()
                        .cardNumber(5419752379558562L)
                        .availableAmount(1000.00)
                        .transactions(
                                Arrays.asList(
                                        Transactions.builder()
                                                .date(LocalDateTime.now())
                                                .amount(1000.00).build()))
                        .build(),
                Card.builder()
                        .cardNumber(5223190090619379L)
                        .availableAmount(1000.00).transactions(
                        Arrays.asList(
                                Transactions.builder()
                                        .date(LocalDateTime.now())
                                        .amount(1000.00).build()))
                        .build()
        ));
    }

}
