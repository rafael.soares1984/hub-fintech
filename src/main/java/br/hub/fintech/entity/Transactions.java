package br.hub.fintech.entity;

import lombok.Builder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Rafael Soares
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TRANSACTIONS")

@SequenceGenerator(name="transaction", sequenceName="trans_seq", initialValue = 1, allocationSize=1)
public class Transactions implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="transaction")
    @Column(name = "TRANSACTION_ID")
    public long id;
    @Column(name = "DATE")
    public LocalDateTime date;
    @Column(name = "AMOUNT")
    public Double amount;

}
