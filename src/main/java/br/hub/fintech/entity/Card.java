package br.hub.fintech.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Rafael Soares
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CARD")
public class Card  implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(name="CARD_ID")
    public Long id;

    @Column(name="CARD_NUMBER")
    public Long cardNumber;

    @Column(name="AVALIABLE_AMOUNT")
    public Double availableAmount;

    @OneToMany(cascade =  CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "TRANSACTION_ID")
    public List<Transactions> transactions;

}
