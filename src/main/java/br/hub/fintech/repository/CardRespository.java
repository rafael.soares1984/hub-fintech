package br.hub.fintech.repository;

import br.hub.fintech.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author Rafael Soares
 *
 */
@Repository
public interface CardRespository extends JpaRepository<Card,Long> {
    @Query("SELECT c FROM Card c WHERE c.cardNumber = :cardNumber")
    public Card findByCardNumber(Long cardNumber);
}
