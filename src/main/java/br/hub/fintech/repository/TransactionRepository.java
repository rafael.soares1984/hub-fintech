package br.hub.fintech.repository;

import br.hub.fintech.entity.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author Rafael Soares
 *
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transactions,Long> {
    @Query(value = "SELECT trans_seq.nextVal  FROM DUAL", nativeQuery = true)
    public Integer latsId();

}
