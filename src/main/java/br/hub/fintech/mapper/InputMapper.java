package br.hub.fintech.mapper;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor

public class InputMapper {

    public String action;
    public Long cardnumber;
    public Double amount;

    public static InputMapper toMapper (StringBuilder content){
        Gson gson = new Gson();
        InputMapper mapper = gson.fromJson(content.toString(), InputMapper.class);
        return mapper;
    }
}
