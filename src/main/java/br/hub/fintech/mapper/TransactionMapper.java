package br.hub.fintech.mapper;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author Rafael Soares
 *
 */

@AllArgsConstructor
@NoArgsConstructor

public class TransactionMapper {

    public LocalDateTime date;
    public Double amount;
}
