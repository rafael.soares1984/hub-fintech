package br.hub.fintech.mapper;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor

public class OutputMapper {

    public String action;
    public String code;
    public Integer authorization_code;

    public static OutputMapper toMapper (StringBuilder content){
        Gson gson = new Gson();
        OutputMapper mapper = gson.fromJson(content.toString(), OutputMapper.class);
        return mapper;
    }
}
