package br.hub.fintech.mapper;

import br.hub.fintech.entity.Card;
import br.hub.fintech.entity.Transactions;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rafael Soares
 *
 */

@AllArgsConstructor
@NoArgsConstructor

public class CardMapper {

    public Long cardNumber;
    public Float availableAmount;
    public List<TransactionMapper> transactions;

    public void setTransactions(List<TransactionMapper> transactions) {
        this.transactions = transactions;
    }
    public CardMapper toMapper(Card Card){
        CardMapper mapper = new CardMapper();
        mapper.cardNumber = Card.getCardNumber();
        List<TransactionMapper> listTransactions = new ArrayList<>();
        for (Transactions transaction : Card.getTransactions()){
            TransactionMapper transactions = new TransactionMapper();
            transactions.amount=transaction.getAmount();
            transactions.date=transaction.getDate();
            listTransactions.add(transactions);
        }
        mapper.transactions= listTransactions;
        return mapper;
    }

}
