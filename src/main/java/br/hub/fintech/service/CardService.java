package br.hub.fintech.service;

import br.hub.fintech.entity.Card;

import java.util.List;

public interface CardService{
    void updateCard(Card card);

    void deleteCard(Long cardNumber);

    Card saveCard(Card card);

    Card getCard(Long cardNumber);

    List<Card> retrieveCards();
}
