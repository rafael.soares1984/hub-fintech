package br.hub.fintech.service;

import br.hub.fintech.entity.Transactions;

public interface TransactionService {
    Transactions saveTransactions(Transactions transactions);

    Integer getLastId();
}
