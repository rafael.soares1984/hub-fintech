package br.hub.fintech.service.impl;

import br.hub.fintech.entity.Card;
import br.hub.fintech.entity.Transactions;
import br.hub.fintech.repository.CardRespository;
import br.hub.fintech.repository.TransactionRepository;
import br.hub.fintech.service.CardService;
import br.hub.fintech.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("CardService")
public class CardImpl implements CardService, TransactionService {

    @Autowired
    private CardRespository cardRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    public CardRespository getCardRepository() {
        return cardRepository;
    }

    @Transactional
    public void updateCard(Card card) {
        cardRepository.save(card);
    }

    @Transactional
    public void deleteCard(Long cardNumber) {
        Card  card = this.getCard(cardNumber);
        for (Transactions custAttrib: card.getTransactions()) {
            transactionRepository.deleteById(custAttrib.getId());
        }
        cardRepository.delete(card);
    }
    @Transactional(readOnly = true)
    public  Card  getCard(Long cardNumber) {
        Card optCust = cardRepository.findByCardNumber(cardNumber);
        return optCust;
    }

    @Transactional(readOnly = true)
    public List<Card> retrieveCards() {
        List <Card> customers = cardRepository.findAll();
        return customers;
    }

    @Transactional
    public Card saveCard(Card Card) {
        cardRepository.save(Card);
        return Card;
    }

    @Transactional
    public Transactions saveTransaction(Transactions transactions) {
        transactionRepository.save(transactions);
        return transactions;
    }

    @Transactional(readOnly = true)
    public List<Transactions> retrieveTransactions() {
        List <Transactions> customers = transactionRepository.findAll();
        return customers;
    }

    @Transactional
    public Transactions saveTransactions(Transactions transactions) {
        Transactions result = null;
        if (!transactions.equals(null)) {
            result = transactionRepository.save(transactions);
        }
        return result;
    }

    @Transactional(readOnly = true)
    public Integer getLastId(){
        return transactionRepository.latsId();
    }
}
