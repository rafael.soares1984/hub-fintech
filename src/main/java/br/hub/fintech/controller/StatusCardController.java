package br.hub.fintech.controller;

import br.hub.fintech.entity.Card;
import br.hub.fintech.entity.Transactions;
import br.hub.fintech.exception.NotFoundException;
import br.hub.fintech.service.CardService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Rafael Soares
 *
 */
@RestController
@Api(value = "Card")
public class StatusCardController {
    public static final Logger logger = LoggerFactory.getLogger(StatusCardController.class);

    @Autowired(required = true)
    private final CardService cardService;

    StatusCardController(CardService cardService) {
        this.cardService = cardService;
    }


    @ApiOperation(value = "Get list of all cards")
    @ApiResponses({
            @ApiResponse(code = 201, response = Card.class, message = "List"),
            @ApiResponse(code = 404, message = "There is no card", response = ApiResponse.class)})
    @GetMapping("/api/card")
    public List<Card> getCards() {
        logger.info("Call api /api/card method GET");
        List<Card> cards = cardService.retrieveCards();
        logger.info("Card : " + cards);
        return cards;
    }

    @ApiOperation(value = "Get the card by id")
    @ApiResponses({
            @ApiResponse(code = 200, response = Card.class, message = "List"),
            @ApiResponse(code = 400, message = "Card id invalid", response = ApiResponse.class),
            @ApiResponse(code = 404, message = "Card not found", response = ApiResponse.class)})
    @GetMapping("/api/card/{cardId}")
    @ResponseHeader(responseContainer = MediaType.APPLICATION_JSON_VALUE)
    public  Card  getCard(@PathVariable(name = "cardId") Long cardId) throws Exception {
        logger.info("Call api /api/card/" + cardId + ") method GET");
        Card card = cardService.getCard(cardId);

        if (!card.equals(null)) {
            return card;
        } else {
            throw new NotFoundException(br.hub.fintech.model.ApiResponse.ERROR, "Card " +  cardId + " not found");
        }
    }

    @ApiOperation(value = "Add Card")
    @ApiResponses({
            @ApiResponse(code = 201, response = Transactions.class, message = "Card")})
    @PostMapping("/api/card")
    @ResponseHeader(responseContainer = MediaType.APPLICATION_JSON_VALUE)
    public Card saveCard(@RequestBody Card card) {
        logger.info("Call api /api/card/ body(" + card.toString() + ") method POST");
        return cardService.saveCard(card);
    }

    @ApiOperation(value = "Delete Card by id")
    @DeleteMapping("/api/card/{cardId}")
    public void deleteCard(@PathVariable(name = "cardId") Long cardeId) {
        cardService.deleteCard(cardeId);
        logger.info("Card deleted with cardId=" + cardeId + " method DELETE");
    }

    @ApiOperation(value = "Update Card by id")
    @ApiResponses({
            @ApiResponse(code = 200, response = Card.class, message = "Card"),
            @ApiResponse(code = 404, message = "Card not found", response = ApiResponse.class)})
    @PutMapping("/api/card/{cardId}")
    public void updateCard(@RequestBody Card card,
                           @PathVariable(name = "cardId") Long cardeId) {
        Card  cus = cardService.getCard(cardeId);
        if (cus != null) {
            logger.info("Call api /api/card/" + cardeId + " body(" + card.toString() + ") method PUT");
            cus.setCardNumber(card.getCardNumber());
            cus.setTransactions(card.getTransactions());
            cardService.updateCard(cus);
        }
        logger.info("Card updated with cardId=" + cardeId + " method PUT");
    }
}