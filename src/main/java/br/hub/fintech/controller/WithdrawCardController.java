package br.hub.fintech.controller;


import br.hub.fintech.entity.Card;
import br.hub.fintech.entity.Transactions;
import br.hub.fintech.mapper.InputMapper;
import br.hub.fintech.mapper.OutputMapper;
import br.hub.fintech.service.CardService;
import br.hub.fintech.service.TransactionService;
import br.hub.fintech.socket.Greeting;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

/**
 * @author Rafael Soares
 *
 */
@Controller
public class WithdrawCardController {

    @Autowired
    private CardService cardService;
    @Autowired
    public TransactionService transactionService;


        private Gson gson = new Gson();
    @MessageMapping("/withdraw")
    @SendTo("/topic/app")
    public Greeting greeting(InputMapper message) throws Exception {
        Thread.sleep(1000);
        OutputMapper out = new OutputMapper();
        out.action = "withdraw";

        Random random = new Random();


        out.authorization_code = random.nextInt(999999);

        out.code =verifiWithdraw(message.cardnumber,message.amount);
        return new Greeting(gson.toJson(out));
    }
    
    public String verifiWithdraw(Long cardNumber, Double amount) throws Exception {

        Card card = cardService.getCard(cardNumber);
        if (card == null){
                return "14";
        }else if (card.availableAmount > amount) {
            cardService.updateCard( Card.builder()
                            .cardNumber(cardNumber)
                            .availableAmount(card.availableAmount - amount)
                            .transactions(
                                    Arrays.asList(transactionService.saveTransactions(
                                            Transactions.builder()
                                                    .date(LocalDateTime.now())
                                                    .amount(amount).build())))
                            .build());
                return "00";
        } else {
            return "51";
        }
    }

}
