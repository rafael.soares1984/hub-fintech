package br.hub.fintech.socket;

import br.hub.fintech.entity.Card;
import br.hub.fintech.entity.Transactions;
import br.hub.fintech.mapper.InputMapper;
import br.hub.fintech.mapper.OutputMapper;
import br.hub.fintech.service.CardService;
import br.hub.fintech.service.TransactionService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

@Component
public class WithdrawHandler extends TextWebSocketHandler {

    private Gson gson;

    @Autowired
    private CardService cardService;
    @Autowired
    public TransactionService transactionService;

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        Thread.sleep(1000);
        InputMapper inMapper = gson.fromJson(message.getPayload(),InputMapper.class);
        OutputMapper ouMapper = new OutputMapper();
        ouMapper.action = inMapper.action;
        ouMapper.authorization_code = new Random().nextInt(999999);
        ouMapper.code = verifiWithdraw(inMapper.cardnumber,inMapper.amount);
        session.sendMessage(new TextMessage(gson.toJson(ouMapper)));
    }

    public String verifiWithdraw(Long cardNumber, Double amount) throws Exception {

        Card card = cardService.getCard(cardNumber);
        if (card == null){
            return "14";
        }else if (card.availableAmount > amount) {
            cardService.updateCard( Card.builder()
                    .cardNumber(cardNumber)
                    .availableAmount(card.availableAmount - amount)
                    .transactions(
                            Arrays.asList(transactionService.saveTransactions(
                                    Transactions.builder()
                                            .date(LocalDateTime.now())
                                            .amount(amount).build())))
                    .build());
            return "00";
        } else {
            return "51";
        }
    }


}
