create table TRANSACTIONS
(
    TRANSACTION_ID INT not null,
    AMOUNT         DECIMAL(10, 2),
    DATE           TIMESTAMP,
    constraint TRANSACTIONS_PK
        primary key (TRANSACTION_ID)
);

