create table CARD
(
    CARD_ID          INTEGER auto_increment,
    AVALIABLE_AMOUNT DECIMAL(10, 2),
    CARD_NUMBER      LONG,
    TRANSACTION_ID   INT,
    constraint CARD_PK
        primary key (CARD_ID),
    constraint CARD_TRANSACTIONS_TRANSACTION_ID_FK
        foreign key (TRANSACTION_ID) references TRANSACTIONS (TRANSACTION_ID)
            on update cascade on delete cascade
);

