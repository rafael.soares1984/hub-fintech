## Test HUB -Fintech

Application was developed by Rafael Soares.

- **Service** [http://localhost:8080/api/card]
- **Swagger** [http://localhost:8080/swagger-ui.html]
- **H2** [http://localhost:8080/h2]
- **Socket** [ws://localhost:8080/withdraw]
# Functionalities of Application

- **GET** [http://localhost:8080/api/card] - List all cards

      GET /api/card/ HTTP/1.1
      Host: localhost:8080
      cache-control: no-cache

- **GET** [http://localhost:8080/api/card/`{cardNumber}`] - Get a specific card

      GET /api/card/1234567890123456 HTTP/1.1
      Host: localhost:8080
      cache-control: no-cache

- **POST** [http://localhost:8080/api/card] Create a new card

  Body ` {
            "cardnumber": "1234567890123456",
            "availableAmount": "1,10",
            "transactions": [
                {
                    "date": "2018-08-08 16:09:01",
                    "amount": "1,10"
                }
            ]
        }`

      POST /api/card HTTP/1.1
      Host: localhost:8080
      Content-Type: application/json
      cache-control: no-cache
      {
            "cardnumber": "1234567890123456",
            "availableAmount": "1,10",
            "transactions": [
                {
                    "date": "2018-08-08 16:09:01",
                    "amount": "1,10"
                }
            ]
        }}------WebKitFormBoundary7MA4YWxkTrZu0gW--

- **PUT** [http://localhost:8080/api/card/`{cardNumber}`] Change a specific customer with customerId

  Body `{
            "cardnumber": "1234567890123456",
            "availableAmount": "1,10",
            "transactions": [
                {
                    "date": "2018-08-08 16:09:01",
                    "amount": "1,10"
                }
            ]
        }`

      PUT /api/customer/100 HTTP/1.1
      Host: localhost:8080
      Content-Type: application/json
      cache-control: no-cache
      {
            "cardnumber": "1234567890123456",
            "availableAmount": "1,10",
            "transactions": [
                {
                    "date": "2018-08-08 16:09:01",
                    "amount": "1,10"
                }
            ]
        }------WebKitFormBoundary7MA4YWxkTrZu0gW--

- **DELETE** [http://localhost:8080/api/card/{cardNumber}] Remove a card

      DELETE /api/card/150 HTTP/1.1
      Host: localhost:8080
      Content-Type: application/json
      cache-control: no-cache

# Tools used for develop application

- **Spring Boot**

  Spring boot was used, because it is easier to group the infrastructures configurations more easily
- **Maven**

  Maven is an excellent package manager, as being the standard of the spring boot
- **Apache Tomcat**

  Apache Tomcat as being the standard for the spring boot, was used
- **H2 Database**

  Since no instructions on database usage were provided, we chose to use H2 Database, which does not require installation, as well as being portable
- **Gitlab**

  Easier to use, I'm already used to it

